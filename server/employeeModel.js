const mongoose = require('mongoose');

const employeeSchema = new mongoose.Schema({
    name:String,
    salary:String,
    position:String,
    email:String,
    phone:String,
    image:String
});

mongoose.model("employee",employeeSchema);

