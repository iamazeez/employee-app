const express = require('express');
const mongoose = require('mongoose');
const bodyParser= require('body-parser');
const app = express();
require('./employeeModel');
const Employee = mongoose.model("employee");

app.use(bodyParser.json());
//app.use(bodyParser);
//Insert your mango db connection link
const DB = "";
mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology:true
  })
  .then(() => console.log('DB connection successful!'))
  .catch(err => console.log('Mongoose connection error : ' + err));


app.get('/', (req,res) =>{
    Employee.find({})
    .then(data => res.send(data))
    .catch(err => console.log(err));
});

app.post('/create', async (req,res) => {
    console.log('Data on request ' + JSON.stringify(req.body));
     await Employee({
        name:req.body.name,
        email:req.body.email,
        phone:req.body.phone,
        salary:req.body.salary,
        position:req.body.salary,
        image:req.body.image
    }).save()
    .then(data => res.send(data))
    .catch(err => console.log(err));
});


app.post('/delete', async (req,res) =>{
    console.log(req.body.id);
    const id = req.body.id;
    await Employee.findOneAndDelete({_id:id})
    .then(data => {console.log(data)
      res.send(data);})
    .catch(err => console.log(err));
    
});
  
app.post('/update',async (req,res) => {
    console.log(req.body.id);
    Employee.findByIdAndUpdate(req.body.id,{
        name:req.body.name,
        email:req.body.email,
        phone:req.body.phone,
        salary:req.body.salary,
        position:req.body.salary,
        image:req.body.image 
    })
    .then(data => res.send(data))
    .catch(err => console.log(err));
    
});

app.listen(3000 , ()=>{
    console.log('Server start');
})