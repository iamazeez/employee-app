/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import Home from './Screens/Home';
import CreateEmployee from './Screens/createEmployee';
import Profile from './Screens/Profile';
import ImageUpload from './Screens/imageUpload';
//import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';


const Stack = createStackNavigator();

const App = () => {
  return (
  
          <View style={styles.hello}>
          <Stack.Navigator>
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="imageUpload" component={ImageUpload} />
          
          <Stack.Screen name="Profile" component={Profile} />
          <Stack.Screen name="Create" component={CreateEmployee} />
          </Stack.Navigator>
          </View>
    
  )
}

const styles = StyleSheet.create({
  hello:{
      flex:1
  },
  cardStyle:{
     width:'100%',
     margin:5,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default () =>{
  return (
  <NavigationContainer>
        <App />
  </NavigationContainer>
  )
}
