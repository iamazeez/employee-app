import React,{useEffect,useState} from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator,Alert, Image } from 'react-native';
import {Card, FAB, Title} from 'react-native-paper';
import ngrok  from './component/ngrok-server';

const Home = (props) => {
    console.log(ngrok)
 
    const [data,setData] = useState([]);
    const [loading,setLoading] = useState(true);

   const fetchData = () => {
    
      fetch(ngrok).then(res => res.json()).then(result => {
         setData(result);
         setLoading(false);
         //console.log(data);
      }).catch(err => {
        console.log('on line 19' + err)
        //Alert.alert('on line 20 ' + 'Something went Wrong ' + err.message);
      });
   
   }
   useEffect(() => {
     fetchData();
  },[]);
    
   const renderList = ((item) => {
    
    return (
        <Card onPress={() => props.navigation.navigate("Profile",item)} style={[style.row,style.mycard, style.boxWithShadow]}>
        <View>
        <Image style={style.roundImage} source={ {uri:item.image} } />
        </View>
        <View>
        <Title style={style.textWhite}>{item.name}</Title>
        <Text style={style.textWhite}>{item.position}</Text>
        </View>
        
       </Card>
    )
   });
   
   return(
       <View style={style.viewStyle}>
           
           {
             loading?
             <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
             <ActivityIndicator size="large" color="#00ff00" />
             </View>
             :<FlatList data={data}
             renderItem={({item}) => {
                 return renderList(item); 
             }}
             keyExtractor={item => item._id}
             onRefresh={()=>fetchData()}
             refreshing={loading}
             />
           }
          
           
         
         

         <FAB
            style={style.fab}
            medium
            icon="plus"
            onPress={() => props.navigation.navigate("Create")}
        />
       

       </View>
   )

}

const style = StyleSheet.create({
   viewStyle:{
      flex:1
   },
   row:{
      flex:1,
      flexDirection:'row'
   },
   roundImage:{
      width:60,
      height:60,
      borderRadius:30
  },
   linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
   mycard:{
       margin:5,
       padding:15,
       backgroundColor:"#8f9fec",
       flexDirection: "row",
   },
   boxWithShadow: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5
},
   textWhite:{
      color:"#fff"
   },
   fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

export default Home;