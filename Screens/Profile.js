import React from 'react';
import { StyleSheet, Text, View,Image, Linking, Platform, Alert } from 'react-native';
//import { LinearGradient } from 'expo-linear-gradient';
import {Card, Button, Title} from 'react-native-paper';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import LinearGradient from "react-native-linear-gradient";
import ngrok  from './component/ngrok-server';
MaterialIcons.loadFont();

const profile = ({route, navigation}) => {
    
    if(!route.params){
        navigation.navigate('Home');
    }
    
    const {_id,name,email,phone,salary,position,image} = route.params;
    console.log(route.params)
    console.log('userID:' + _id)
    
    const openDial = () => {
        if(Platform.OS === 'android'){
            Linking.openURL(`tel:${phone}`);
        }else{
            Linking.openURL(`telprompt:${phone}`);
        }
    }

    const deleteEmploye = (userId) => {
        console.log('userID in delete :' + userId)
        const url = ngrok + '/delete';
        fetch(url,{
            method:"post",
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                id:userId
            })
        })
        .then(res=>res.json())
        .then(deletedEmp =>{
            Alert.alert(`${deletedEmp.name} deleted`)
            props.navigation.navigate("Home")
        })  
        .catch(err=>{
            console.log('Profile page : ' + err);
            Alert.alert("Something went wrong")
          })

      /*  Alert.alert(
            "Are sure to fire?",
            "Press okay to fire employee",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "OK", onPress: () => {
                  fetch(`http://127.0.0.1:3000/delete/`,{
                      method:'post',
                      header:{
                        'Content-Type':'application/json'
                      },
                      body:JSON.stringify({
                          id:_id
                      })
                  }).then(res => res.json()).then(
                      data => {console.log(data);
                      navigation.navigate('Home');
                      }
                  ).catch(err => console.log(err));
              } }
            ]
          );*/
    }
  

     const styles = StyleSheet.create({
         container:{
            flex:1,
            
         },
         linearGradient: {
            paddingLeft: 15,
            paddingRight: 15,
            borderRadius: 5
          },
        roundImage:{
            width:100,
            height:100,
            borderRadius:50
        },
        imageView:{
            alignItems:'center',
            marginTop:25,
           
        },
        introView:{
            alignItems:'center',
            marginTop:10,
            marginBottom:10
        },
        cardStyle:{
            padding:10,
            margin:5,
            backgroundColor:'#dbdbdb',
        },
        cardContent:{
            flexDirection:'row',
            alignItems:'center'
        },
        buttonView:{
            justifyContent:'space-around',
            flexDirection:'row',
            marginTop:10
        }
     });

     return (
<View style={styles.container}>
<LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={styles.linearGradient}>    
<View style={styles.imageView}>
<Image style={styles.roundImage} source={ {uri:image} } />
</View>
<View style={styles.introView}>
<Title style={{color:'#fff'}}>{name}</Title>
<Text style={{color:'#fff'}}>{position}</Text>
</View>
</LinearGradient>
             
   


<Card style={[styles.cardStyle, {marginTop:20}]} onPress={() => Linking.openURL(`mailto:${email}`)}>
    <View style={styles.cardContent}>
    <MaterialIcons name='email' size={32} color='#ad39a5' /><Text style={{marginLeft:10}}>{email}</Text>
    </View>
</Card>

<Card style={styles.cardStyle} onPress={ openDial }>
    <View style={styles.cardContent}>
    <MaterialIcons name='phone' size={32} color='#ad39a5' /><Text style={{marginLeft:10}}>{phone}</Text>
    </View>
</Card>

<Card style={styles.cardStyle}>
    <View style={styles.cardContent}>
    <MaterialIcons name='attach-money' size={32} color='#ad39a5' /><Text style={{marginLeft:10}}>$ {salary}</Text>
    </View>
</Card>

<View style={styles.buttonView}>
<Button icon='upload' mode='contained' onPress={() => navigation.navigate("Create",route.params)}> Edit </Button>
<Button icon='upload' mode='contained' onPress={() => deleteEmploye(_id)}> Fire Employee </Button>
</View>

 </View>
     )
}

export default profile;