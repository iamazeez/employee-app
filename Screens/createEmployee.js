import React,{useState} from 'react';
import { StyleSheet, Text, View ,Modal, Alert} from 'react-native';
import {TextInput,Button} from 'react-native-paper';
import ImagePicker from 'react-native-image-crop-picker';
import ngrok from './component/ngrok-server';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker/src';

const styles = StyleSheet.create({

   margin5:{
       margin:5
   },
   modelStyle:{
    position: 'absolute',
    backgroundColor:'rgba(0,0,0,0.2)',
    width:"100%",
    bottom: 2,
   },
   modelButtonView:{
       flexDirection:'row',
       justifyContent:'space-around',
       padding:10,
   }
});

const theme = {
    color:{
        primary:'#006aff'
    }
}

const CreateEmployee = (props) => {
    
    console.log('Props in createEmp : ' + JSON.stringify(props.route.params))

    const getData = (data) => {
        if(props.route.params){
            switch(data){
                case 'name' : return props.route.params.name;
                case 'email' : return props.route.params.email;
                case 'phone' : return props.route.params.phone;
                case 'salary' : return props.route.params.salary;
                case 'position' : return props.route.params.position;
                case 'image' : return props.route.params.image;
            }
        }

        return "";
    }

    const [Name,setName] = useState(getData('name'));
    const [Email,setEmail] = useState(getData('email'));
    const [phone,setPhone] = useState(getData('phone'));
    const [salary,setSalary] = useState(getData('salary'));
    const [position,setPosition] = useState(getData('position'));
    const [image,setImage] = useState(getData('image'));
    const [modal,setModal] = useState(false);

    const gallaryUpload = () => {
        const options = {
          title: 'Select Photo',
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        launchImageLibrary(options, (response) => {
    
          // console.log('Response = ', response);
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else {
            const uri = response.uri;
            const type = response.type;
            const name = response.fileName;
            const source = {
              uri,
              type,
              name,
            }
            cloudinaryUpload(source)
          }
        });
      }


      const cloudinaryUpload = (photoSource) => {
        const data = new FormData()
        data.append('file', photoSource)
        data.append('upload_preset', 'employeeApp')
        data.append("cloud_name", "dcjrertzh")
        fetch("https://api.cloudinary.com/v1_1/dcjrertzh/upload", {
          method: "post",
          body: data
        }).then(res => res.json()).
          then(datas => {
            setImage(datas.secure_url)
          }).catch(err => {
            Alert.alert("An Error Occured While Uploading")
          })
      }
     

    
    

    
    const updateEmployee = (userId) => {
        const url = ngrok + '/update';
        fetch(url , {
            method:'post',
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                id:userId,
                name:Name,
                email:Email,
                phone:phone,
                salary:salary,
                position:position,
                image:image
            })
        }).then(res => res.json()).then(data => {
            console.log('Employee Updated : ' + data);
            Alert.alert(`${data.name} updated!`)
        })
        .catch(err => console.log(err));
        setModal(false)
    }
    

    const submitData = async () => {
        const url = ngrok + '/create';
        await fetch(url , {
            method:'post',
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                name:Name,
                email:Email,
                phone:phone,
                salary:salary,
                position:position,
                image:image
            })
        }).then(res => res.json()).then(data => {
            console.log('Create Employee : ' + data);
            Alert.alert(`${data.name} saved!`)
        })
        .catch(err => console.log(err));
        setModal(false)
    } 

    const updateData = async () => {
        const url = ngrok + '/update/' + props.route.params._id;
        await fetch(url , {
            method:'post',
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                name:Name,
                email:Email,
                phone:phone,
                salary:salary,
                position:position,
                image:image
            })
        }).then(res => res.json()).then(data => {
            Alert.alert(`${data.name} updated!`)
        })
        .catch(err => console.log(err));
        setModal(false)
    }

    const openCamera = async () => {
        await ImagePicker.openCamera({
            width: 400,
            height: 400,
            cropping: true,
          }).then(image => {
            console.log(image);
          }).catch(err => console.log(err));
          setModal(false)
    }

    const openGallery = async () => {
        await ImagePicker.openPicker({    
            width: 300,
            height: 400,
            cropping: true
          }).then(image => {
              console.log('Image : ' + JSON.stringify(image))
            uploadImage(image)
          }).catch(err => console.log(err));
          setModal(false)
    }  

    const uploadImage = async (image) => {
        const data = new FormData();
        data.append('file',image);
        data.append('upload_preset','employeeApp');
        data.append('cloud_name','dcjrertzh');

        await fetch("https://api.cloudinary.com/v1_1/dcjrertzh/image/upload" ,{
            method:'post',
            body:data
        }).then(res => res.json())
        .then(datas => {console.log(datas)
         console.log('in data')
        })
        .catch(err => {
            console.log(err)
            console.log('In the error');
        });
    }

    return (
        <View>

<View style={styles.margin5}>
<TextInput mode='outlined' theme={theme} label='Name' value={Name} onChangeText={text => setName(text)} />
<TextInput mode='outlined' label='Email' value={Email} onChangeText={text => setEmail(text)} />
<TextInput keyboardType='phone-pad' mode='outlined' label='phone' value={phone} onChangeText={text => setPhone(text)} />
<TextInput mode='outlined' label='salary' value={salary} onChangeText={text => setSalary(text)} />
<TextInput mode='outlined' label='Position' value={position} onChangeText={text => setPosition(text)} />
</View>
             
<Button style={styles.margin5} icon='upload' mode='contained' onPress={() => setModal(true)}> Upload Image </Button>

{props.route.params?
    <Button style={styles.margin5} icon='content-save' mode='contained' onPress={() => updateEmployee(props.route.params._id)}> Update </Button>
    :
    <Button style={styles.margin5} icon='content-save' mode='contained' onPress={() => submitData()}> Save </Button>
}


            
<Modal animationType='slide' transparent={true} visible={modal} onRequestClose={() => setModal(false)} >
<View style={styles.modelStyle}>
    <View style={styles.modelButtonView} >
    <Button style={styles.margin5} icon='content-save' mode='contained' onPress={() => openCamera()}> Camera </Button>
    <Button style={styles.margin5} icon='content-save' mode='contained' onPress={() => gallaryUpload()}> Gallary </Button>
    </View>
    <Button style={styles.margin5} icon='content-save' mode='contained' onPress={() => setModal(false)}> Cancel </Button>
</View>

</Modal>


        </View>
    )
}

export default CreateEmployee;
